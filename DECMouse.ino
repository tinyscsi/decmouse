#include <ps2.h>

/*
 * an arduino sketch to interface with a ps/2 mouse.
 * Also uses serial protocol to talk back to the host
 * and report what it finds.
 */

uint8_t DECmode = 'R';

/*
 * Pin 5 is the mouse data pin, pin 6 is the clock pin
 * Feel free to use whatever pins are convenient.
 */
PS2 mouse(6, 5);

/*
 * initialize the mouse. Reset it, and place it into remote
 * mode, so we can get the encoder data on demand.
 */
void mouse_init()
{
  mouse.write(0xff);  // reset
  mouse.read();  // ack byte
  mouse.read();  // blank */
  mouse.read();  // blank */
  mouse.write(0xf0);  // remote mode
  mouse.read();  // ack
  delayMicroseconds(100);
}

void setup()
{
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(150);
  Serial1.begin(4800, SERIAL_8O1);
  delay(150);
  mouse_init();
  digitalWrite(13, LOW);
  if(Serial) Serial.print("Init\r\n");
  Serial1.write(0xA0);  // Self Test Report, REV0
  Serial1.write(0x02);  // Manufacturing ID 0, Mouse
  Serial1.write(0x00);  // No Errors
  Serial1.write(0x00);  // No Buttons Held
}

/*
 * get a reading from the mouse and report it back to the
 * host via the serial line.
 */
elapsedMillis eLED;
int tLED = 0;
void loop()
{
  char mstat;
  char mx;
  char my;

  if(eLED > 300) {
    eLED = 0;
    tLED = !tLED;
    digitalWrite(13, tLED);
  }

  if(Serial1.available()) {
    uint8_t incomingByte = Serial1.read();
    switch(incomingByte) {
      case 'R':
        DECmode = 'R';
        if(Serial) Serial.print("Stream Mode\r\n");
        break;
      case 'P':
        DECmode = 'P';
        if(Serial) Serial.print("Poll Read\r\n");
        break;
      case 'D':
        DECmode = 'D';
        if(Serial) Serial.print("Poll Mode\r\n");
        break;
      case 'T':
        DECmode = 'T';
        if(Serial) Serial.print("Self Test\r\n");
        break;
      default:
        if(Serial) {
          Serial.print(incomingByte, HEX);
          Serial.print(" Unknown Byte\r\n");
        }
        break;
    }
  }

  switch(DECmode) {
    case 'R':
    case 'P':
      /* get a reading from the mouse */
      mouse.write(0xeb);  // give me data!
      mouse.read();      // ignore ack
      mstat = mouse.read();
      mx = mouse.read();
      my = mouse.read();
      if(mx & 0x80) mx ^= 0x7f;
      if(my & 0x80) my ^= 0x7f;
      mx ^= 0x80;
      my ^= 0x80;
      // DEC Serial Mouse Packet
      Serial1.write(0x80 | ((mx & 0x80)>>3) | ((my & 0x80) >> 4) | ((mstat & 0x1) << 2) | (mstat & 0x2) | ((mstat & 0x4) >> 2));
      Serial1.write(mx & 0x7f);
      Serial1.write(my & 0x7f);
      if(DECmode == 'P') DECmode = 'D';
      break;
    case 0xff:
    case 'T':
      Serial1.write(0xA0);  // Self Test Report, REV0
      Serial1.write(0x02);  // Manufacturing ID 0, Mouse
      Serial1.write(0x00);  // No Errors
      Serial1.write(0x00);  // No Buttons Held
      if(DECmode == 'T')
        DECmode = 'D';
      break;
  }
  
  delay(5);
}


